# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# [1.4.0] - 2018-10-13
## Change
- Optional Dependencies:
    - org.manzocad:dependencies:1.4.0

# [1.3.0] - 2018-06-06
## Add
- Mandatory Dependencies:
    - org.springframework.boot:spring-boot-starter:2.0.2.RELEASE
## Removed
- Mandatory Dependencies:
    - org.springframework.boot:spring-boot-starter-web:1.5.9.RELEASE

# [1.0.0] - 2018-03-07
## Change
- manzocad dependencies to version 1.3.0


# [1.0.0] - 2018-03-07
## Add
- Parent project: org.springframework.boot:spring-boot-starter-parent:1.5.9.RELEASE
- Optional Dependencies:
    - org.manzocad:dependencies:1.0.0
    - org.manzocad.spring:shiro-spring-boot-starter:0.2.0
    - org.manzocad.spring:nitrite-spring-boot-starter:0.2.0
- Mandatory Dependencies:
    - org.springframework.boot:spring-boot-starter-web:1.5.9.RELEASE
- Plugin:
    - org.springframework.boot:spring-boot-maven-plugin:1.5.9.RELEASE