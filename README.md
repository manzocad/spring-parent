# Manzocad Spring Boot Parent

This project provides a starting point for a Spring Boot Webapp project.
It extends the spring-boot-starter-parent including optional dependencies
from org.manzocad:dependencies as well manzocad starters. It also includes
mandatory dependency to org.springframework.boot:spring-boot-starter-web.

    <!-- Add Manzocad repository to pom -->
    <repositories>
        <repository>
            <id>bintray-manzo-giuseppe-manzocad-maven-repository</id>
            <url>https://dl.bintray.com/manzo-giuseppe/manzocad-maven-repository</url>
        </repository>
    </repositories>

    <!-- Set spring-bom as the parent of this project -->
    <parent>
        <groupId>org.manzocad</groupId>
        <artifactId>spring-parent</artifactId>
        <version>${spring.parent.version}</version>
    </parent>